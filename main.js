class Card {
    constructor(post, user) {
      this.post = post;
      this.user = user;
      this.element = this.createCardElement();
    }

    createCardElement() {
      const cardElement = document.createElement('div');
      cardElement.className = 'card';
      cardElement.id = `card-${this.post.id}`;

      const titleElement = document.createElement('h2');
      titleElement.textContent = this.post.title;

      const bodyElement = document.createElement('p');
      bodyElement.textContent = this.post.body;

      const authorElement = document.createElement('p');
      authorElement.textContent = `Posted by: ${this.user.name} ${this.post.id}`;

      
      const emailElement = document.createElement('p');
      emailElement.textContent = `Email: ${this.user.email}`;

      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Delete';
      deleteButton.addEventListener('click', () => {
        this.deletePublication();
      });

      cardElement.appendChild(titleElement);
      cardElement.appendChild(bodyElement);
      cardElement.appendChild(authorElement);
      cardElement.appendChild(emailElement);
      cardElement.appendChild(deleteButton);

      return cardElement;
    }

    deletePublication() {


      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
        method: 'DELETE'
      })
        .then(response => console.log(response))
        .then(data => {
          console.log('Publication deleted:', data);
          this.element.remove(); // Remove the card from the DOM
        })
        .catch(error => {
          console.error('Error deleting publication:', error);
        });
    }
  }

  function displayPublications(users, posts) {
    const publicationsContainer = document.getElementById('container');

    posts.forEach(post => {
      const user = users.find(u => u.id === post.userId);
      if (user) {
        const card = new Card(post, user);
        publicationsContainer.appendChild(card.element);
      }
    });
  }

  // Fetch users and posts from the server
  fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
      fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(posts => {
          // Display publications on the page
          displayPublications(users, posts);
        })
        .catch(error => {
          console.error('Error fetching posts:', error);
        });
    })
    .catch(error => {
      console.error('Error fetching users:', error);
    });
